import pytest
from core.constants import MODERATION_MODE_MODERATORS
from core.models import ExternalRoom, SchedulingStrategy, Server, get_default_room_config
from core.services import external_get_or_create_room_with_params


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def example_server(db, example) -> Server:
    return Server.objects.create(
        scheduling_strategy=example,
        dns="example.org",
        shared_secret="123456789",
    )


@pytest.fixture(scope="function")
def external_room(db, example) -> ExternalRoom:
    return ExternalRoom.objects.create(
        scheduling_strategy=example,
        meeting_id="99999999",
        name="D14/02.14",
        attendee_pw="test_attendee_password_external",
        moderator_pw="test_moderator_password_external",
        moderation_mode=MODERATION_MODE_MODERATORS,
        config=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def external_room_params(db):
    return {
        "name": "test_external_room_name",
        "meetingID": "123-456-789",
        "attendeePW": "test_attendee_password",
        "attendeePW": "test_attendee_password",
        "moderatorPW": "test_moderator_password",
    }


def test_external_get_room_with_params(external_room_params, external_room, example):
    existing_external_room = external_get_or_create_room_with_params(external_room_params, example)
    external_room = ExternalRoom.objects.get(meeting_id="123-456-789")
    assert existing_external_room == external_room
    external_room_params["meetingID"] = "8465132123468432"
    existing_external_room = external_get_or_create_room_with_params(external_room_params, example)
    assert ExternalRoom.objects.all().count() == 3
    assert existing_external_room.name != external_room.name
