import json
from functools import wraps

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.sites.shortcuts import get_current_site
from django.db import DataError
from django.http import Http404, HttpResponse, HttpResponseNotAllowed
from django.shortcuts import redirect, render
from support_chat.models import PreparedAnswer, SupportChatParameter, Supporter
from support_chat.utils import has_supporter_privileges


def support_chat_enabled(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if SupportChatParameter.load(get_current_site(request)).enabled:
            return view_func(request, *args, **kwargs)

        raise Http404

    return _wrapped_view


@support_chat_enabled
@login_required
@user_passes_test(has_supporter_privileges)
def chats_overview(request) -> HttpResponse:
    _tenant = get_current_site(request)
    prepared_answers = [
        {"title": prepared_answer.title, "text": prepared_answer.text}
        for prepared_answer in PreparedAnswer.objects.filter(tenant=_tenant)
    ]

    return render(request, "support_chat/chats_overview.html", {"prepared_answers": json.dumps(prepared_answers)})


@support_chat_enabled
@login_required
@user_passes_test(has_supporter_privileges)
def prepared_answers_overview(request) -> HttpResponse:
    _tenant = get_current_site(request)
    if request.method == "GET":
        return render(
            request,
            "support_chat/prepared_answers_overview.html",
            {"prepared_answers": list(PreparedAnswer.objects.filter(tenant=_tenant))},
        )
    elif request.method == "POST":
        try:
            if "id" in request.POST:
                prepared_answers = PreparedAnswer.objects.get(id=int(request.POST["id"]))

                if "title" in request.POST and "text" in request.POST:
                    prepared_answers.title = request.POST["title"]
                    prepared_answers.text = request.POST["text"]
                    prepared_answers.save()
                else:
                    prepared_answers.delete()
            else:
                PreparedAnswer.objects.create(title=request.POST["title"], text=request.POST["text"], tenant=_tenant)
        except (ValueError, KeyError, PreparedAnswer.DoesNotExist, DataError):
            pass
        return redirect("support_chat_prepared_answers_overview")
    else:
        return HttpResponseNotAllowed(["GET", "POST"])


@support_chat_enabled
@login_required
@user_passes_test(has_supporter_privileges)
def supporters_overview(request) -> HttpResponse:
    active_supporters = []
    inactive_supporters = []
    _tenant = get_current_site(request)
    for supporter in Supporter.objects.prefetch_related("id").filter(id__tenant=_tenant):
        if supporter.is_active:
            active_supporters.append(str(supporter.user))
        else:
            inactive_supporters.append(str(supporter.user))

    return render(
        request,
        "support_chat/supporters_overview.html",
        {"active_supporters": active_supporters, "inactive_supporters": inactive_supporters},
    )


@login_required
@staff_member_required
def settings(request) -> HttpResponse:
    support_chat_parameter = SupportChatParameter.load(get_current_site(request))
    if request.method == "GET":
        return render(
            request,
            "support_chat/settings.html",
            {
                "enabled": support_chat_parameter.enabled,
                "disable_chat_if_offline": support_chat_parameter.disable_chat_if_offline,
                "message_max_length": support_chat_parameter.message_max_length,
            },
        )
    elif request.method == "POST":
        try:
            support_chat_parameter.enabled = "enabled" in request.POST
            support_chat_parameter.disable_chat_if_offline = "disableChatIfOffline" in request.POST
            support_chat_parameter.message_max_length = int(request.POST["messageMaxLength"])
            support_chat_parameter.save()
        except (ValueError, KeyError):
            pass
        return redirect("support_chat_settings")
    else:
        return HttpResponseNotAllowed(["GET", "POST"])
