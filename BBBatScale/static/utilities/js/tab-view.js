/**
 * A little helper class to handle TabViews.
 *
 * @param {HTMLElement} tabView
 * @param {HTMLElement} tabOverview
 * @return {TabView}
 * @constructor
 */
function TabView(tabView, tabOverview) {
    if (!(this instanceof TabView)) {
        return new TabView(tabView, tabOverview);
    }

    /**
     * @type {?HTMLElement}
     */
    let activeTab = null;

    /**
     * Displays the overview.
     */
    this.showOverview = function () {
        tabOverview.classList.remove("hidden");
    }

    /**
     * Hides the overview.
     */
    this.hideOverview = function () {
        tabOverview.classList.add("hidden");
    }

    /**
     * Displays the supplied tab-pane and hides the overview.
     *
     * @param {HTMLElement} tabPane
     * @return {?HTMLElement} The previous active tab-pane.
     */
    this.showTab = function (tabPane) {
        const previousTab = activeTab;

        if (activeTab !== tabPane) {
            if (activeTab) {
                this.hideTab()
            }

            tabPane.classList.add("active");
            activeTab = tabPane;
        }

        this.hideOverview();

        return previousTab;
    }

    /**
     * Hides the current tab-pane and displays the overview.
     *
     * @return {boolean} True if there was an active tab and it has been hidden, otherwise false.
     */
    this.hideTab = function () {
        this.showOverview();

        if (activeTab) {
            activeTab.classList.remove("active");
            activeTab = null;
            return true;
        }

        return false;
    }
}
