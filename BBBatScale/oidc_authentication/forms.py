from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field, Layout, Submit
from django import forms
from django.utils.translation import gettext_lazy as _

from .models import AuthParameter


class AuthParameterForm(forms.ModelForm):
    class Meta:
        model = AuthParameter
        fields = [
            "tenant",
            "client_id",
            "client_secret",
            "authorization_endpoint",
            "token_endpoint",
            "user_endpoint",
            "jwks_endpoint",
            "end_session_endpoint",
            "scopes",
            "verify_ssl",
            "sign_algo",
            "logout_url_method",
            "idp_sign_key",
            "refresh_enabled",
            "renew_id_token_expiry_seconds",
            "state_size",
            "use_nonce",
            "nonce_size",
            "allow_unsecured_jwt",
            "timeout",
            "create_user",
            "oidc_username_standard",
            "claim_unique_username_field",
        ]

    def __init__(self, *args, **kwargs):
        super(AuthParameterForm, self).__init__(*args, **kwargs)

        self.fields["tenant"].label = _("Tenant")
        self.fields["client_id"].label = _("Client ID")
        self.fields["client_secret"].label = _("Client secret")
        self.fields["authorization_endpoint"].label = _("Authorization endpoint")
        self.fields["token_endpoint"].label = _("Token endpoint")
        self.fields["user_endpoint"].label = _("User endpoint")
        self.fields["jwks_endpoint"].label = _("JWKS endpoint")
        self.fields["end_session_endpoint"].label = _("End session endpoint")
        self.fields["scopes"].label = _("Scopes")
        self.fields["verify_ssl"].label = _("Verify SSL")
        self.fields["sign_algo"].label = _("Sign algo")
        self.fields["logout_url_method"].label = _("Logout url method")
        self.fields["idp_sign_key"].label = _("IDP sign key")
        self.fields["refresh_enabled"].label = _("refresh enabled")
        self.fields["renew_id_token_expiry_seconds"].label = _("Renew id token expiry seconds")
        self.fields["state_size"].label = _("state size")
        self.fields["use_nonce"].label = _("Use nonce")
        self.fields["nonce_size"].label = _("Nonce size")
        self.fields["allow_unsecured_jwt"].label = _("Allow unsecured jwt")
        self.fields["timeout"].label = _("Timeout")
        self.fields["create_user"].label = _("Create user")
        self.fields["oidc_username_standard"].label = _("Use oidc username standard")
        self.fields["claim_unique_username_field"].label = _("Claim unique username field")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("tenant", readonly=True, diabled=True),
            Field("client_id"),
            Field("client_secret"),
            Field("authorization_endpoint"),
            Field("token_endpoint"),
            Field("user_endpoint"),
            Field("jwks_endpoint"),
            Field("end_session_endpoint"),
            Field("scopes"),
            Field("sign_algo"),
            Field("verify_ssl"),
            Field("logout_url_method"),
            Field("idp_sign_key"),
            Field("refresh_enabled"),
            Field("renew_id_token_expiry_seconds"),
            Field("state_size"),
            Field("use_nonce"),
            Field("nonce_size"),
            Field("allow_unsecured_jwt"),
            Field("timeout"),
            Field("create_user"),
            Field("oidc_username_standard"),
            Field("claim_unique_username_field"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )
