import logging

from core.filters import UserFilter
from core.forms import (
    PasswordChangeCrispyForm,
    SchedulingStrategyFilterFormHelper,
    UserForm,
    UserFormWithPassword,
    UserResetPasswordAdminForm,
    UserSettings,
)
from core.models import User
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.sites.shortcuts import get_current_site
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@staff_member_required
def users_overview(request):
    logger.info(create_view_access_logging_message(request))

    f = UserFilter(request.GET, queryset=User.objects.filter(tenant=get_current_site(request)))
    f.form.helper = SchedulingStrategyFilterFormHelper
    return render(request, "users_overview.html", {"filter": f})


@login_required
@staff_member_required
def user_create(request):
    logger.info(create_view_access_logging_message(request))

    form = UserFormWithPassword(request.POST or None)
    if request.method == "POST" and form.is_valid():
        user = form.save(commit=False)
        user.tenant = get_current_site(request)
        user.set_password(user.password)
        user.save()
        form.save_m2m()
        messages.success(request, _("User {} successfully created.").format(user.username))
        # return to URL
        return redirect("users_overview")
    return render(request, "user_create.html", {"form": form})


@login_required
@staff_member_required
def user_delete(request, user):
    logger.info(create_view_access_logging_message(request, user))

    instance = get_object_or_404(User, pk=user)
    instance.delete()
    messages.success(request, _("User successfully deleted."))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@staff_member_required
def user_update(request, user):
    logger.info(create_view_access_logging_message(request, user))

    instance = get_object_or_404(User, pk=user)

    if request.method == "POST":
        form = UserForm(request.POST, instance=instance)
        if form.is_valid():
            _user = form.save()
            messages.success(request, _("User {} successfully updated").format(_user.username))
            return redirect("users_overview")
    form = UserForm(instance=instance, initial={"groups": instance.groups.all()})
    return render(request, "user_create.html", {"form": form})


@login_required
def user_settings(request):
    logger.info(create_view_access_logging_message(request))

    if request.method == "POST":
        form = UserSettings(request.POST, instance=request.user)
        if form.is_valid():
            _user = form.save()
            messages.success(request, _("User settings successfully updated").format(_user.username))
            return redirect("user_settings")
    form = UserSettings(instance=request.user)
    return render(request, "user_settings.html", {"form": form})


@login_required
@user_passes_test(lambda user: user.has_usable_password())
def user_change_password(request):
    logger.info(create_view_access_logging_message(request))

    form = PasswordChangeCrispyForm(request.user, request.POST or None)
    if request.method == "POST" and form.is_valid():
        user = form.save()
        update_session_auth_hash(request, user)
        messages.success(request, _("Password successfully changed."))
        return redirect("home")

    return render(request, "password_change.html", {"form": form})


@login_required
@staff_member_required
@user_passes_test(lambda user: user.has_usable_password())
def user_change_password_admin(request, user):
    logger.info(create_view_access_logging_message(request, user))
    instance = get_object_or_404(User, pk=user)
    form = UserResetPasswordAdminForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            _user = form.save(commit=False)
            if form.cleaned_data["password"] == form.cleaned_data["password_confirm"]:
                _user.set_password(form.cleaned_data["password"])
                _user.save()
                messages.success(request, _("User {} successfully changed password").format(_user.username))
                return redirect("users_overview")
            else:
                messages.error(request, _("Passwords did not match"))
    return render(request, "user_create.html", {"form": form})


@login_required
def user_search_json(request):
    logger.info(create_view_access_logging_message(request))

    q = request.GET.get("q")
    users = []
    if q and len(q) >= 2:
        for user in User.objects.filter(Q(first_name__icontains=q) | Q(last_name__icontains=q) | Q(email__icontains=q)):
            users.append({"id": user.id, "display_name": str(user)})
    return JsonResponse({"users": users})
