import os

from core.management.commands import collect_all_room_occupancy
from core.models import Room, RoomEvent, SchedulingStrategy, Server, get_default_room_config
from django.test import TestCase
from freezegun import freeze_time


class CollectRoomOccupancy(TestCase):
    def setUp(self):
        self.fbi = SchedulingStrategy.objects.create(
            name="FBI",
        )

        self.bbb_server = Server.objects.create(
            scheduling_strategy=self.fbi,
            dns="example.org",
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.rooms = []

        file_path = (
            "file://" + os.path.dirname(os.path.realpath(__file__)) + "/test_data/SimpleICalCollector_testdata.ics"
        )
        self.rooms.append(
            Room.objects.create(
                scheduling_strategy=self.fbi,
                server=self.bbb_server,
                name="D14/02.04",
                participant_count=10,
                videostream_count=5,
                event_collection_strategy="SimpleICalCollector",
                event_collection_parameters='{"iCal_url": "' + file_path + '", "iCal_encoding": "UTF-8"}',
                config=get_default_room_config(),
            )
        )
        self.rooms.append(
            Room.objects.create(
                scheduling_strategy=self.fbi,
                server=self.bbb_server,
                name="D14/02.05",
                participant_count=10,
                videostream_count=5,
                event_collection_strategy="SimpleICalCollector",
                event_collection_parameters='{"iCal_url": "' + file_path + '", "iCal_encoding": "UTF-8"}',
                config=get_default_room_config(),
            )
        )

    @freeze_time("2020-05-25 06:30:00", tz_offset=0)
    def test_command_real_data(self):
        collect_all_room_occupancy.Command().handle()

        room_events = RoomEvent.objects.all()
        self.assertEqual(len(room_events), 4)
