# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-22 11:19+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: core/static/home/js/updateRooms.js:144
msgid "active"
msgstr ""

#: core/static/home/js/updateRooms.js:147
msgid "inactive"
msgstr ""

#: core/static/home/js/updateRooms.js:150
msgid "creating"
msgstr ""

#: static/utilities/js/misc.js:159
msgid "The URL has successfully been copied to the clipboard."
msgstr ""

#: static/utilities/js/misc.js:161
msgid "Your Browser does not support copying to the clipboard."
msgstr ""
