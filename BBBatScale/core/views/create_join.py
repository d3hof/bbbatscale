import hmac
import logging
import time
from typing import Dict, Optional, Union
from urllib.parse import quote

from core.constants import ROOM_STATE_ACTIVE, ROOM_STATE_CREATING, ROOM_STATE_INACTIVE
from core.forms import ConfigureRoomForm
from core.models import GeneralParameter, Room
from core.services import (
    create_join_meeting_url,
    get_join_password,
    join_or_create_meeting_permissions,
    meeting_create,
    room_click,
)
from core.views import create_view_access_logging_message
from django.conf import settings
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.db.transaction import atomic, non_atomic_requests
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


class JoinParameters:
    def __init__(self, valid_secret: bool = False, direct_join: bool = False) -> None:
        self.valid_secret = valid_secret
        self.direct_join = direct_join

    @staticmethod
    def validate_session_join_parameters(
        meeting_id: str, join_parameters: Optional[Dict[str, Union[int, str, bool]]]
    ) -> "JoinParameters":
        if (
            join_parameters
            and join_parameters["meeting_id"] == meeting_id
            and ((join_parameters["timestamp"] + 300_000) > round(time.time() * 1000))
        ):
            return JoinParameters(join_parameters["valid_secret"], join_parameters["direct_join"])
        return JoinParameters()


def join_redirect(request, room):
    logger.info(create_view_access_logging_message(request))

    try:
        pk, meeting_id, secret = Room.objects.values_list("pk", "meeting_id", "direct_join_secret").get(name=room)
        room_click(pk)
        request.session["join_parameters"] = {
            "meeting_id": meeting_id,
            "timestamp": round(time.time() * 1000),
            "valid_secret": False,
            "direct_join": False,
        }
        if secret and request.GET.get("joinSecret") == secret:
            logger.debug("Retrieved valid join secret for meeting %s", meeting_id)
            request.session["join_parameters"]["valid_secret"] = True
        if secret and request.GET.get("directJoin", "").lower() == "true":
            logger.debug("Retrieved direct join for meeting %s", meeting_id)
            request.session["join_parameters"]["direct_join"] = True

        return redirect("join_or_create_meeting", meeting_id)
    except Room.DoesNotExist:
        messages.error(request, _("Unable to find Room with given name: '{}'.").format(room))
        return redirect("home")


def join_or_create_meeting(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    _room = get_object_or_404(Room, meeting_id=meeting_id)
    join_parameters = JoinParameters.validate_session_join_parameters(
        _room.meeting_id, request.session.get("join_parameters")
    )

    if request.user.is_anonymous and not join_parameters.valid_secret:
        if not _room.allow_guest_entry:
            messages.warning(
                request, _("The meeting is not allowed for guest entry. Please login to access the meeting.")
            )
            logger.debug(
                "user is not authenticated and not room.allow_guest_entry; 'The meeting"
                " is not allowed for guest entry. Please login to access the meeting.' redirect to 'login'"
            )
            return redirect(settings.LOGIN_URL + "?next=" + quote(request.get_full_path_info()))

        if "username" not in request.session:
            logger.debug(
                "user is not authenticated and 'username' not in request.session; redirect to set_username_pre_join"
            )
            return redirect("set_username_pre_join", meeting_id)

    if _room.state == ROOM_STATE_INACTIVE and (
        join_or_create_meeting_permissions(request.user, _room) or join_parameters.valid_secret
    ):
        logger.debug(
            "room.state is INACTIVE and permissions for user=(%s) are sufficient; redirect to 'create_meeting'"
        )
        return redirect("create_meeting", _room.meeting_id)

    logger.debug("Redirect to 'join_meeting'")
    return redirect("join_meeting", _room.meeting_id)


@non_atomic_requests
def create_meeting(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    logger.debug("Query Database Object and lock it for further requests!")
    instance = get_object_or_404(Room, meeting_id=meeting_id)
    join_parameters = JoinParameters.validate_session_join_parameters(
        instance.meeting_id, request.session.get("join_parameters")
    )
    general_parameter = GeneralParameter.load(get_current_site(request))
    if request.user.is_anonymous and "username" not in request.session and not join_parameters.valid_secret:
        logger.debug(
            "user is not authenticated and username is not in request.session; "
            + "redirect to 'set_username_pre_join', meeting_id=%s",
            meeting_id,
        )
        return redirect("set_username_pre_join", meeting_id)

    if not join_or_create_meeting_permissions(request.user, instance) and not join_parameters.valid_secret:
        logger.debug("user has no join_or_create_meeting_permissions; redirect to 'join_meeting'")
        return redirect("join_meeting", instance.meeting_id)

    form = ConfigureRoomForm(
        request.POST or None, instance=instance, enable_recordings=general_parameter.enable_recordings
    )
    if request.user.is_authenticated:
        creator_name = request.user.username
    elif "username" in request.session:
        creator_name = request.session["username"]
    else:
        creator_name = instance.name
    logger.debug("creator_name set to %s", creator_name)

    if (request.method == "POST" and form.is_valid()) or join_parameters.direct_join:
        _room = instance if join_parameters.direct_join else form.save(commit=False)
        with atomic():
            logger.debug("Start transactions for room creation")
            if (
                Room.objects.filter(meeting_id=meeting_id, state=ROOM_STATE_INACTIVE).update(
                    state=ROOM_STATE_CREATING, participant_count=1, last_running=now()
                )
                >= 1
            ):
                meeting_create(
                    request,
                    {
                        "name": instance.name,
                        "meeting_id": instance.meeting_id,
                        "attendee_pw": instance.attendee_pw,
                        "moderator_pw": instance.moderator_pw,
                        "mute_on_start": _room.mute_on_start,
                        "moderation_mode": _room.moderation_mode,
                        "everyone_can_start": _room.everyone_can_start,
                        "access_code": _room.access_code,
                        "only_prompt_guests_for_access_code": _room.only_prompt_guests_for_access_code,
                        "disable_cam": _room.disable_cam,
                        "disable_mic": _room.disable_mic,
                        "disable_note": _room.disable_note,
                        "disable_private_chat": _room.disable_private_chat,
                        "disable_public_chat": _room.disable_public_chat,
                        "allow_recording": _room.allow_recording if general_parameter.enable_recordings else False,
                        "guest_policy": _room.guest_policy,
                        "url": _room.url,
                        "creator": creator_name,
                        "allow_guest_entry": _room.allow_guest_entry,
                        "logoutUrl": _room.logoutUrl,
                        "dialNumber": _room.dialNumber,
                        "welcome_message": _room.welcome_message,
                        "maxParticipants": _room.maxParticipants,
                        "streamingUrl": _room.streamingUrl,
                    },
                )
                return redirect("join_meeting", _room.meeting_id)
            else:
                messages.warning(
                    request,
                    _(
                        "The meeting was started by another moderator."
                        " You will be redirected to the meeting shortly."
                    ),
                )
                logger.debug(
                    "Meeting was started by another moderator in parallel; " + "redirect to 'join_or_create_meeting'"
                )

                return redirect("join_or_create_meeting", instance.meeting_id)

    logger.debug("Redirect to configure_room_for_meeting due to invalid form")
    return render(request, "configure_room_for_meeting.html", {"form": form, "room": instance})


def join_meeting(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    _room = get_object_or_404(Room, meeting_id=meeting_id)
    join_parameters = JoinParameters.validate_session_join_parameters(
        _room.meeting_id, request.session.get("join_parameters")
    )

    user_needs_access_code = _room.access_code and (
        not _room.only_prompt_guests_for_access_code or request.user.is_anonymous
    )

    username = request.user.username
    if request.user.is_anonymous and not join_parameters.valid_secret:
        if not _room.allow_guest_entry:
            messages.warning(request, _("The moderator did not allow guest entry. Please login to access the meeting."))
            logger.debug(
                "The moderator did not allow guest entry. Please login to access the meeting; redirect to 'login'"
            )
            return redirect(settings.LOGIN_URL + "?next=" + quote(request.get_full_path_info()))
        elif "username" in request.session:
            username = request.session["username"]
        elif not join_parameters.valid_secret:
            return redirect("join_or_create_meeting", _room.meeting_id)

    if join_parameters.valid_secret and not username:
        username = _room.name

    if _room.state == ROOM_STATE_ACTIVE:
        logger.debug("Room.state is ACTIVE")

        password = (
            _room.moderator_pw if join_parameters.valid_secret else get_join_password(request.user, _room, username)
        )

        join_user = request.user if request.user.is_authenticated else username
        if user_needs_access_code:
            access_code = request.POST.get("access_code", None)
            if access_code:
                if hmac.compare_digest(access_code.encode("UTF-8"), _room.access_code.encode("UTF-8")):
                    request.session.pop("username", None)
                    return redirect(create_join_meeting_url(_room.meeting_id, join_user, password))
                else:
                    messages.error(request, _("Incorrect access code."))
        else:
            request.session.pop("username", None)
            return redirect(create_join_meeting_url(_room.meeting_id, join_user, password))

    # Meeting is not running or access code is needed
    context = {"room": _room, "demand_access_code": user_needs_access_code and _room.state == ROOM_STATE_ACTIVE}
    return render(request, "join_meeting.html", context)


def session_set_username(request):
    logger.info(create_view_access_logging_message(request))

    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])
    request.session["username"] = request.POST.get("username")
    return HttpResponse("ok")


def session_set_username_pre_join(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    if request.user.is_authenticated or "username" in request.session:
        return redirect("join_or_create_meeting", meeting_id)
    return render(request, "set_username.html", {"meeting_id": meeting_id})


def get_meeting_status(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    _room = get_object_or_404(Room, meeting_id=meeting_id)
    logger.debug("Was asked for state of {}. Answer is {}".format(_room, _room.state))
    return JsonResponse({"is_running": _room.state == ROOM_STATE_ACTIVE})
