from core.views.create_join import join_redirect
from core.views.home import home
from core.views.recordings import recording_redirect
from django.conf import settings
from django.conf.urls import include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.sites.shortcuts import get_current_site
from django.urls import path
from mozilla_django_oidc.views import OIDCAuthenticationCallbackView
from oidc_authentication.models import AuthParameter
from oidc_authentication.views import (
    TenantOIDCAuthenticationRequestView,
    TenantOIDCLogoutView,
    auth_parameter_create,
    auth_parameter_delete,
    auth_parameter_update,
)


def login_view(request):
    return (
        TenantOIDCAuthenticationRequestView.as_view()(request)
        if AuthParameter.objects.filter(tenant=get_current_site(request)).exists()
        else auth_views.LoginView.as_view(
            template_name="login.html",
            redirect_authenticated_user=True,
            extra_context={"login_url": request.get_full_path_info()},
        )(request)
    )


def logout_view(request):
    return (
        TenantOIDCLogoutView.as_view()(request)
        if AuthParameter.objects.filter(tenant=get_current_site(request)).exists()
        else auth_views.LogoutView.as_view(
            next_page="home",
        )(request)
    )


urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
    path("admin/", admin.site.urls),
    path("core/", include("core.urls")),
    path("login", login_view, name="login"),
    path("logout", logout_view, name="logout"),
    # Since the OIDC middleware expects the logout url name oidc_logout and the view does not provide a naming list
    # this needs to be included
    path("logout", logout_view, name="oidc_logout"),
    path("", home, name="home"),
    path("r/<path:room>", join_redirect, name="join_redirect"),
    path("p/<str:replay_id>", recording_redirect, name="recording_redirect"),
    path("support/", include("support_chat.urls")),
    path("login/callback", OIDCAuthenticationCallbackView.as_view(), name="oidc_authentication_callback"),
    path("login/authenticate", TenantOIDCAuthenticationRequestView.as_view(), name="oidc_authentication_init"),
    path("tenant/authparameter/create/<int:tenant>", auth_parameter_create, name="auth_parameter_create"),
    path("tenant/authparameter/update/<int:tenant>", auth_parameter_update, name="auth_parameter_update"),
    path("tenant/authparameter/delete/<int:tenant>", auth_parameter_delete, name="auth_parameter_delete"),
]

if settings.WEBHOOKS_ENABLED:
    urlpatterns.append(path("django-rq/", include("django_rq.urls")))
